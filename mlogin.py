#!/usr/bin/python3
# -*- coding: utf-8 -*-
import gi, os, locale, pwd, json, socket
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf, GtkLayerShell, GObject

translate = {"tr":{0:"Kullanıcı Adı",
					1:"Parola",
					2:"Giriş",
					3:"Yanlış Parola",
					4:"Giriş Yapılıyor"},
			"en":{0:"User Name",
					1:"Password",
					2:"Login",
					3:"Wrong password",
					4:"Logging In"}}

l = locale.getdefaultlocale()
l = l[0].split("_")[0]
locales = list(translate.keys())
if l not in locales:
	l = "en"
_ = translate[l]

g_socket = os.getenv("GREETD_SOCK")
client = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
client.connect(g_socket)

class MLoginWindow(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self)
		box = Gtk.Fixed()
		self.add(box)
		y = SIZE.height/2

		background_image = Gtk.Image()
		background_image.set_size_request(SIZE.width, SIZE.height)
		pb = GdkPixbuf.Pixbuf.new_from_file_at_scale("/home/trlinux/mlogin/default.jpg",SIZE.width, SIZE.height,False)
		background_image.set_from_pixbuf(pb)
		box.put(background_image,0, 0)

		context = self.get_style_context()
		color = context.get_background_color(Gtk.StateFlags.NORMAL)
		print(color.to_string())
		bg_svg = """<svg width="{}" height="{}"><rect width="{}" height="{}" style="fill:rgb({},{},{})" /></svg>""".format(450,200,450,200,int(color.red*255),int(color.green*255),int(color.blue*255))
		print(color,bg_svg)
		loader = GdkPixbuf.PixbufLoader()
		loader.set_size(450,200)
		loader.write(bg_svg.encode())
		loader.close()
		pb = loader.get_pixbuf()
		item_image = Gtk.Image()
		item_image.set_size_request(450, 200)
		item_image.set_from_pixbuf(pb)
		box.put(item_image,25,y-85)

		sessions = os.listdir("/usr/share/wayland-sessions/")
		self.wm_combo = Gtk.ComboBoxText()
		for s in sessions:
			self.wm_combo.append_text(s.replace(".desktop",""))
		self.wm_combo.set_active(0)
		self.wm_combo.set_size_request(310, 30)
		box.put(self.wm_combo,50,y-60)

		default_theme = Gtk.IconTheme.get_default()
		pb = default_theme.load_icon("system-reboot",24,Gtk.IconLookupFlags.FORCE_SIZE)
		img = Gtk.Image()
		img.set_from_pixbuf(pb)
		self.reboot_button = Gtk.Button()
		self.reboot_button.connect("clicked",self.reboot)
		self.reboot_button.set_image(img)
		self.reboot_button.set_size_request(30, 30)
		box.put(self.reboot_button,370,y-60)

		default_theme = Gtk.IconTheme.get_default()
		pb = default_theme.load_icon("system-shutdown",24,Gtk.IconLookupFlags.FORCE_SIZE)
		img = Gtk.Image()
		img.set_from_pixbuf(pb)
		self.poweroff_button = Gtk.Button()
		self.poweroff_button.connect("clicked",self.poweroff)
		self.poweroff_button.set_image(img)
		self.poweroff_button.set_size_request(30, 30)
		box.put(self.poweroff_button,410,y-60)

		user_name_lbl = Gtk.Label()
		user_name_lbl.set_size_request(140, 30)
		user_name_lbl.set_text(_[0])
		box.put(user_name_lbl,50,y-20)

		users = self.get_users()
		self.user_name_entry = Gtk.ComboBoxText()
		for s in users:
			self.user_name_entry.append_text(s)
		self.user_name_entry.set_active(0)
		self.user_name_entry.set_size_request(260, 30)
		box.put(self.user_name_entry,190,y-20)

		self.user_passwd_lbl = Gtk.Label()
		self.user_passwd_lbl.set_size_request(140, 30)
		self.user_passwd_lbl.set_text(_[1])
		box.put(self.user_passwd_lbl,50,y+20)
		self.user_passwd_entry = Gtk.Entry()
		self.user_passwd_entry.set_visibility(False)
		self.user_passwd_entry.set_size_request(260, 30)
		box.put(self.user_passwd_entry,190,y+20)

		self.login_button = Gtk.Button()
		self.login_button.connect("clicked",self.login_func)
		self.login_button.set_label(_[2])
		self.login_button.set_size_request(400, 30)
		box.put(self.login_button,50,y+60)

		self.connect("key-press-event",self.key_press)
		self.user_passwd_entry.grab_focus()

	def poweroff(self,widget):
		os.system("sudo poweroff")

	def reboot(self,widget):
		os.system("sudo reboot")

	def key_press(self, widget, event):
		key_name = Gdk.keyval_name(event.keyval)
		if key_name == "Return":
			self.login_func(None)

	def g_send(self, json_req):
		print(f"greetd: request = {json_req}")
		req = json.dumps(json_req)
		client.send(len(req).to_bytes(4, "little") + req.encode("utf-8"))
		resp_raw = client.recv(128)
		resp_len = int.from_bytes(resp_raw[0:4], "little")
		resp_trimmed = resp_raw[4:resp_len + 4].decode()
		try:
			r = json.loads(resp_trimmed)
			print(f"greetd: response = {r}")
			return r
		except ValueError:
			print(f"greetd: ValueError")
			return {}


	def login_func(self, widget):
		jreq = {"type": "cancel_session"}
		p0 = self.g_send(jreq)
		jreq = {"type": "create_session", "username": self.user_name_entry.get_active_text() }
		p1 = self.g_send(jreq)
		print(p1)
		jreq = {"type": "post_auth_message_response", "response": self.user_passwd_entry.get_text()}
		p2 = self.g_send(jreq)
		print(p2)
		if "type" in p2 and p2["type"] == "success":
			self.user_passwd_lbl.set_text(_[4])
			self.login_button.set_label(_[4])
			self.login_button.set_sensitive(False)
			self.poweroff_button.set_sensitive(False)
			self.reboot_button.set_sensitive(False)
			self.user_passwd_entry.set_sensitive(False)
			self.user_name_entry.set_sensitive(False)
			self.wm_combo.set_sensitive(False)
			print("#### Dogru Sifre")
			jreq = {"type": "start_session", "cmd": ["dinit --wm {}".format(self.wm_combo.get_active_text())] }
			sonuc = self.g_send(jreq)
			print(sonuc)
		else:
			self.user_passwd_lbl.set_text(_[3])
			self.login_button.set_label(_[3])
			self.login_button.set_sensitive(False)
			GObject.timeout_add(1000, self.return_buttons)

	def return_buttons(self):
		self.user_passwd_lbl.set_text(_[1])
		self.login_button.set_label(_[2])
		self.login_button.set_sensitive(True)
		return False

	def get_users(self):
		a_users = []
		users = pwd.getpwall()
		for u in users:
			if u.pw_uid >= 1000:
				a_users.append(u.pw_name)
		return a_users


screen = Gdk.Display.get_default()
monitors = screen.get_n_monitors()
active_monitor = screen.get_monitor(0)
SIZE = active_monitor.get_geometry()
win = MLoginWindow()
GtkLayerShell.init_for_window(win)
GtkLayerShell.set_margin(win, GtkLayerShell.Edge.TOP, 0)
GtkLayerShell.set_margin(win, GtkLayerShell.Edge.BOTTOM, 0)
GtkLayerShell.set_margin(win, GtkLayerShell.Edge.LEFT, 0)
GtkLayerShell.set_margin(win, GtkLayerShell.Edge.RIGHT, 0)
GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.TOP, 1)
GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.BOTTOM, 1)
GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.LEFT, 1)
GtkLayerShell.set_anchor(win, GtkLayerShell.Edge.RIGHT, 1)
GtkLayerShell.set_keyboard_mode(win, GtkLayerShell.KeyboardMode.EXCLUSIVE)
win.show_all()
Gtk.main()
